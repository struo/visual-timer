import 'package:flutter/material.dart';
import 'package:visual_timer/app.dart';
import 'package:wakelock/wakelock.dart';


Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await FirebaseAdMob.instance.initialize(appId: (kReleaseMode) ? "ca-app-pub-7521403426743253~4929567403": FirebaseAdMob.testAppId);
  Wakelock.enable();
  runApp(App());
}
