import 'dart:async';

import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:awesome_circular_chart/awesome_circular_chart.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:visual_timer/Painter/ClockDialPainter.dart';

class TimerPage extends StatefulWidget {
  TimerPage({Key? key, required this.initialTimer}) : super(key: key);

  final double initialTimer;

  @override
  _TimerState createState() => _TimerState(timer: this.initialTimer);
}

class _TimerState extends State<TimerPage> {
  // BannerAd? _bannerAd;
  double fullTimer = 3600;
  double timer;

  late List<DropdownMenuItem<int>> minuteDropDownItems;
  final GlobalKey<AnimatedCircularChartState> _chartKey = new GlobalKey<AnimatedCircularChartState>();

  /// Constructor
  _TimerState({required this.timer}) {
    Timer.periodic(Duration(seconds: 1), tickTimer);
    this.createAndShowBannerAd();
    final List<int> minutesList = [
      0,
      1,
      2,
      3,
      4,
      5,
      10,
      15,
      20,
      30,
      45,
      60
    ];
    minuteDropDownItems=minutesList.map<DropdownMenuItem<int>>((int value) {
      return DropdownMenuItem<int>(
        value: value,
        child: Text(value.toString() + " minutes"),
      );
    }).toList();

  }

  // static const MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo(
  //   // testDevices: <String>["661ED6A4A6227505468D882F18565880"],
  //   childDirected: true,
  //   nonPersonalizedAds: true,
  // );

  void createAndShowBannerAd() {
    // if (this._bannerAd == null) {
    //   this._bannerAd = BannerAd(
    //     adUnitId: (kReleaseMode) ? "ca-app-pub-7521403426743253/8965119090" : BannerAd.testAdUnitId,
    //     size: AdSize.banner,
    //     targetingInfo: targetingInfo,
    //     listener: (MobileAdEvent event) {
    //       print("BannerAd event $event");
    //     },
    //   );
    //   this._bannerAd
    //     ?.load().whenComplete(() => this._bannerAd?.show(anchorOffset: kToolbarHeight, anchorType: AnchorType.top,));
    // }
  }

  void tickTimer(Timer tick) {
    if (this.timer > 0) {
      timer -= 1;
      setState(() {
        _chartKey.currentState?.updateData(generateChartData());
      });
      if (timer == 0) {
        SystemChrome.setEnabledSystemUIOverlays(
            [SystemUiOverlay.bottom, SystemUiOverlay.top]);
        this.createAndShowBannerAd();
        AssetsAudioPlayer.newPlayer().open(
          Audio("assets/audio/alarm_01.wav"),
          autoStart: true,
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    double size;
    if (MediaQuery.of(context).size.width >
        MediaQuery.of(context).size.height) {
      size = MediaQuery.of(context).size.height + 75;
    } else {
      size = MediaQuery.of(context).size.width + 75;
    }

    FloatingActionButton? actionButton;
    if (timer == 0) {
      actionButton = FloatingActionButton(
        backgroundColor: Colors.green[600],
        onPressed: _timerDialog,
        tooltip: 'Increment',
        child: Icon(Icons.play_arrow),
      );
    }

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Stack(
              children: <Widget>[
                AnimatedCircularChart(
                  key: _chartKey,
                  size: Size(size, size),
                  initialChartData: generateChartData(),
                  chartType: CircularChartType.Pie,
                  percentageValues: false,
                  duration: Duration(microseconds: 100),
                ),
                new Container(
                  width: size,
                  height: size,
                  padding: const EdgeInsets.all(10.0),
                  child: new CustomPaint(
                    painter: new ClockDialPainter(),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      floatingActionButton: actionButton,
    );
  }

  List<CircularStackEntry> generateChartData() {
    double red = timer;
    double yellow = 0;
    double green = 0;

    if (red > 180) {
      yellow = red - 180;
      red = 180;
    }

    if (yellow > 300) {
      green = yellow - 300;
      yellow = 300;
    }

    return <CircularStackEntry>[
      new CircularStackEntry(
        <CircularSegmentEntry>[
          new CircularSegmentEntry(
            green,
            Colors.green[700],
            rankKey: 'green_color',
          ),
          new CircularSegmentEntry(
            yellow,
            Colors.yellow[700],
            rankKey: 'yellow_color',
          ),
          new CircularSegmentEntry(
            red,
            Colors.red[700],
            rankKey: 'red_color',
          ),
          new CircularSegmentEntry(
            fullTimer - timer,
            Colors.black,
            rankKey: 'completed',
          ),
        ],
        rankKey: 'progress',
      ),
    ];
  }

  int? tempMinutes = 0;

  // int? tempSeconds = 0;

  Future<void> _timerDialog() async {
    // this._bannerAd?.dispose();
    // this._bannerAd = null;
    showDialog(
      context: context,
      builder: (context) => StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {

          return new AlertDialog(
            title: new Text("Timer duration"),
            content: Container(
              height: 100,
              child: Column(
                children: <Widget>[
                  DropdownButton<int>(
                    value: tempMinutes,
                    icon: Icon(Icons.arrow_drop_down),
                    iconSize: 24,
                    elevation: 16,
                    style: TextStyle(color: Colors.black, fontSize: 18),
                    onChanged: (int? value) => {
                      setState(() {
                        tempMinutes = value;
                      })
                    },
                    underline: Container(
                      height: 2,
                      color: Colors.black,
                    ),
                    items: this.minuteDropDownItems,
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Start timer'),
                onPressed: () {
                  SystemChrome.setEnabledSystemUIOverlays(
                      [SystemUiOverlay.bottom]);
                  _setTimer((tempMinutes! * 60.0));
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        },
      ),
    );
  }

  void _setTimer(double timer) {
    this.setState(() {
      this.timer = timer;
      _chartKey.currentState?.updateData(generateChartData());
    });
  }
}
